import Vue from 'vue';
import Axios from 'axios';
import VueAxios from 'vue-axios';

export default () => {
  Vue.use(VueAxios, Axios);
}
